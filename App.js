import React from 'react';
import { StyleSheet, View } from 'react-native';
import { ApplicationProvider,IconRegistry, Layout, Text } from '@ui-kitten/components';
import { mapping, light as lightTheme } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { FeatherIconsPack } from './feather-icons';
import  AppNavigator  from './navigation';
export default function App() {
  return (
    
    <ApplicationProvider  mapping={mapping} theme={lightTheme} >
      <Layout style={{flex: 1, textAlign: '-webkit-center',backgroundColor: '#f7f7f7'}}>
      <Layout style={styles.container}>
        <IconRegistry icons={[EvaIconsPack, FeatherIconsPack]} />
     <AppNavigator />
     </Layout>
     </Layout>
  </ApplicationProvider>
     
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: '480px',
    backgroundColor: '#fff',
  },
});

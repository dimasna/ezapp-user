import React from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity,ScrollView } from 'react-native';
import { Divider, Layout, Icon, Text } from '@ui-kitten/components';
import SearchBox from '../component/searchBox';
import CardCarousel from '../component/cardCarousel';
import TaskList from '../component/taskList';


export const Home = ({ navigation }) => {

  const navigateDetails = () => {
    navigation.navigate('Details');
  };



  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView nestedScrollEnabled={true}>
      <Layout style={{ flex: 1, alignItems: 'stretch', flexDirection: 'column' }}>
          <Layout style={{ alignItems: 'flex-end', flexDirection: 'row-reverse', paddingTop: '1.5em', paddingRight: '1.3em' }}>
            <TouchableOpacity onPress={() => {console.log('notif clicked')}}>
              <Icon style={{ width: '24px', fill: '#8F9BB3' }} name="bell" />
            </TouchableOpacity>
          </Layout>
          <Layout style={{alignItems: 'stretch', flexDirection: 'row',marginTop: '0.5em'}}>
              <Layout style={{flex: 3,alignItems: 'flex-start', flexDirection: 'column'}}>
              
                <Text style={styles.text} category='label'>Hello Dimas</Text>
                <Text style={styles.text2} category='p1'>what are you up today?</Text>
               
                

              </Layout>
              <Layout style={{flex: 3,alignItems: 'center', flexDirection: 'row-reverse', alignSelf: 'center'}}>
              
                  
                  <TouchableOpacity onPress={() => {console.log('notif clicked')}}>
                  
                  <Text  style={styles.coin} category='s1'>1.785</Text>
                </TouchableOpacity>
                <Icon name='coins' pack="fontAwesome" style={{ color: '#ffc107', fontSize: '35', marginRight: '0.5em'}} solid/>
                    

              </Layout>
          </Layout>
          
     
      <SearchBox/>
      <CardCarousel/>
       
      <Divider style={{height: '0.5em', backgroundColor: '#f7f7f7', marginTop: '1em', marginBottom: '1em'}}/>
      <TaskList/>
      </Layout>
        
     </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#353131',
    fontSize: 20,
    paddingLeft: '1.3em'
  },
  text2: {
    color: '#acacac',
    fontSize: 12,
    flex: 3,
    textAlign: 'left',
    marginTop: '0.5em',
    paddingLeft: '2.1em'
  },
  coin: {
    color: '#353131',
    fontSize: 20,
    flex: 3,
    textAlign: 'right',
    paddingRight: '1.2em',
        
  }
});
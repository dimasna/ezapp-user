import React from 'react';
import { StyleSheet } from 'react-native';
import {
  BottomNavigation,
  BottomNavigationTab,
  Icon,
  Layout,
  Divider
} from '@ui-kitten/components';

const HomeIcon = (style) => (
  <Icon {...style} name='home'/>
);

const StatusIcon = (style) => (
  <Icon {...style} name='shopping-bag'/>
);

const PostingIcon = (style) => (
  <Icon {...style} name='plus-circle'/>
);
const FeedsIcon = (style) => (
  <Icon {...style} name='grid'/>
);
const AccountIcon = (style) => (
  <Icon {...style} name='person'/>
);

const BottomNavigationWithIconsShowcase = () => {


  const [bottomSelectedIndex, setBottomSelectedIndex] = React.useState(0);

  return (
    <Layout>

      <BottomNavigation
        appearance='noIndicator'
        style={styles.bottomNavigation}
        selectedIndex={bottomSelectedIndex}
        onSelect={setBottomSelectedIndex}>
        <BottomNavigationTab title='Home' icon={HomeIcon}/>
        <BottomNavigationTab title='Status' icon={StatusIcon}/>
        <BottomNavigationTab  title='Posting' status='posting'  icon={PostingIcon}/>
        <BottomNavigationTab title='Feeds' icon={FeedsIcon}/>
        <BottomNavigationTab title='Account' icon={AccountIcon}/>
      </BottomNavigation>

    </Layout>
  );
};

const styles = StyleSheet.create({
  bottomNavigation: {
    marginVertical: 8,
    borderTopColor: '#cdcdcd',
    borderTopWidth: 1
  }
});

export default BottomNavigationWithIconsShowcase;
import React from 'react';
import {
  Button,
  Icon,
  List,
  ListItem,
  Divider,
  Avatar
} from '@ui-kitten/components';

const data = new Array(28).fill({
  title: 'Ajarin buat aplikasi android',
  description: 'Lorem ipsum dolar sit atmet Lorem ipsum dolar sit atmet Lorem ipsum dolar sit atmet',
});

 const ListCompositeItemShowcase = () => {

  const renderItemAccessory = (style) => (
    <Button style={style}>BID</Button>
  );

  const renderItemIcon = (style) => (
    <Icon {...style} name='person'/>
  );

  const renderItem = ({ item, index }) => (
      <>
    <ListItem
      title={`${item.title} ${index + 1}`}
      description={`${item.description} ${index + 1}`}
     
      icon={renderItemIcon}
      accessory={renderItemAccessory}
    >
      
    </ListItem>
    <Divider/>
    </>
  );

  return (
    <List
      data={data}
      renderItem={renderItem}
      nestedScrollEnabled={true}
    />
  );
};

export default ListCompositeItemShowcase;

import React from 'react';
import { StyleSheet } from 'react-native';
import {
  Autocomplete,
  Icon,
  Layout,
} from '@ui-kitten/components';

const CloseIcon = (style) => (
  <Icon {...style} name='close'/>
);
const SearchIcon = (style) => (
  <Icon {...style} name='search-outline'/>
);


const DATA = [
  {
    id: 1,
    title: 'Star Wars',
    releaseYear: 1977,
  },
  {
    id: 2,
    title: 'Back to the Future',
    releaseYear: 1985,
  },
  {
    id: 3,
    title: 'The Matrix',
    releaseYear: 1999,
  },
  {
    id: 4,
    title: 'Inception',
    releaseYear: 2010,
  },
  {
    id: 5,
    title: 'Interstellar',
    releaseYear: 2014,
  },
];

 const AutocompleteWithIconShowcase = () => {

  const [value, setValue] = React.useState(null);
  const [data, setData] = React.useState(DATA);

  const onSelect = ({ title }) => {
    setValue(title);
  };

  const onChangeText = (query) => {
    setValue(query);
    setData(DATA.filter(item => item.title.toLowerCase().includes(query.toLowerCase())));
  };

  const clearInput = () => {
    setValue('');
    setData(DATA);
  };

  return (
    <Layout style={styles.container}>
      <Autocomplete
        placeholder='Cari tugas yang bisa kamu bantu'
        value={value}
        data={data}
        icon={ value ? CloseIcon : SearchIcon}
        onIconPress={clearInput}
        onChangeText={onChangeText}
        onSelect={onSelect}
      />
    </Layout>
  );
};

const styles = StyleSheet.create({
  container: {
    marginRight: '0.8em',
    marginLeft: '1.5em',
    marginTop: '2em'
  },
});

export default AutocompleteWithIconShowcase;
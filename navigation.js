import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, View } from 'react-native';
import { Layout, Text } from '@ui-kitten/components';
import { Home } from './src/pages/home';
import { Status } from './src/pages/status';
import BottomNav from './src/component/bottomNavigation';

const Stack = createStackNavigator();

const HomeNavigator = () => (
  <Stack.Navigator headerMode='none'>
    
    <Stack.Screen name='Home' component={Home}/>
    <Stack.Screen name='Details' component={Status}/>
  </Stack.Navigator>
);

const AppNavigator = () => (
  
  <NavigationContainer >
   
    <HomeNavigator/>

    <BottomNav/>
    
    
  </NavigationContainer>

);

export default AppNavigator;